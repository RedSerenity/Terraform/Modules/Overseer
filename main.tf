data "docker_registry_image" "Overseer" {
	name = "sctx/overseerr:latest"
}

resource "docker_image" "Overseer" {
	name = data.docker_registry_image.Overseer.name
	pull_triggers = [data.docker_registry_image.Overseer.sha256_digest]
}

module "Overseer" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Overseer.latest

	networks = [{ name: var.docker_network, aliases: ["overseer.${var.internal_domain_base}"] }]
  ports = [{ internal: 5055, external: 9609, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/Overseer"
			container_path = "/app/config"
			read_only = false
		}
	]

	environment = {
		"PUID": "${var.uid}",
		"PGID": "${var.gid}",
		"TZ": "${var.tz}"
	}

	stack = var.stack
}